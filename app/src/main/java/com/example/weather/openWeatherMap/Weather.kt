package com.example.weather.openWeatherMap

class Weather(val description: String,
              val iconURL: String,
              val temperature: Float,
              val pressure: Int,
              val humidity: Int) {

}
