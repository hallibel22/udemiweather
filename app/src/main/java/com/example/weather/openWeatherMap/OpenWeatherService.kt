package com.example.weather.openWeatherMap

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

private const val API_KEY = "08a24a8afe6973e69cfdb90583b6ef7b"

interface OpenWeatherService {

    @GET("data/2.5/weather?unit=metric&lang=fr")
    fun getWeather(@Query("q") cityName: String,
                   @Query("appid") apiKey: String = API_KEY) : Call<WeatherWrapper>
}