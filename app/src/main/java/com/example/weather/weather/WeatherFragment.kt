package com.example.weather.weather

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.weather.App
import com.example.weather.R
import com.example.weather.openWeatherMap.Weather
import com.example.weather.openWeatherMap.WeatherWrapper
import com.example.weather.openWeatherMap.mapOpenWeatherDataToWeather
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherFragment : Fragment() {

    private lateinit var cityName: String

    private lateinit var city: TextView
    private lateinit var weatherIcon: ImageView
    private lateinit var weatherDescription: TextView
    private lateinit var humidity: TextView
    private lateinit var temperature: TextView
    private lateinit var pressure: TextView

    private lateinit var refresh: SwipeRefreshLayout

    companion object {

        val EXTRA_CITY_NAME = "com.example.weather.weather.EXTRA_CITY_NAME"

        fun newinstance() = WeatherFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_weather, container, false)

        city = view.findViewById(R.id.city)
        pressure = view.findViewById(R.id.pressure)
        temperature = view.findViewById(R.id.temp)
        weatherDescription = view.findViewById(R.id.weatherdescription)
        weatherIcon = view.findViewById(R.id.weathericon)
        humidity = view.findViewById(R.id.humidity)

        refresh = view.findViewById(R.id.swipe_refresh)
        refresh.setOnRefreshListener { refreshWeather() }

        return view
    }

    private fun refreshWeather() {
        updateWeatherForCity(cityName)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(activity?.intent!!.hasExtra(EXTRA_CITY_NAME)) {
            updateWeatherForCity(activity!!.intent.getStringExtra(EXTRA_CITY_NAME))
        }
    }

    fun updateWeatherForCity(cityName: String) {
       this.cityName = cityName

        if(!refresh.isRefreshing)
            refresh.isRefreshing = true

        val call = App.weatherService.getWeather("$cityName, fr")
        call.enqueue(object : Callback<WeatherWrapper> {
            override fun onFailure(call: Call<WeatherWrapper>, t: Throwable) {
                Log.e("WeatherFragment", "could not load", t)
                Toast.makeText(activity,"Fail to load", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<WeatherWrapper>,
                response: Response<WeatherWrapper>
            ) {
                response?.body()?.let {
                    val weather = mapOpenWeatherDataToWeather(it)
                    updateUI(weather)
                    Log.i("WeatherFragment", "response : ${weather}")
                }
            }

        })
        refresh.isRefreshing = false
    }

    private fun updateUI(weather: Weather) {
        weatherDescription.text = weather.description
        temperature.text = getString(R.string.weather_temperature_value, weather.temperature.toInt() - 273)
        humidity.text = getString(R.string.weather_humidity_value, weather.humidity)
        pressure.text = getString(R.string.weather_pressure_value, weather.pressure)
        Picasso.get()
            .load(weather.iconURL)
            .placeholder(R.drawable.ic_cloud_off_black_24dp)
            .into(weatherIcon)

    }

    fun clearUI() {
        weatherIcon.setImageResource(R.drawable.ic_cloud_off_black_24dp)
        cityName = ""
        city.text = ""
        weatherDescription.text = ""
        temperature.text = ""
        humidity.text = ""
        pressure.text = ""
    }
}