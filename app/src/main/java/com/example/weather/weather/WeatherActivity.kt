package com.example.weather.weather

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.weather.R

class WeatherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, WeatherFragment.newinstance())
            .commit()
    }
}
