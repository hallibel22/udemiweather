package com.example.weather.city

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.InputType
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.example.weather.R

class CreateCityDialogFragment : DialogFragment(){

    interface CreateCityDialogListener {
        fun onDialogPositiveClick(cityName: String)
        fun onDialogNegativeClick()
    }

    var listener: CreateCityDialogListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var dialogBuilder = AlertDialog.Builder(context)

        val input = EditText(context)
        with(input) {
            inputType = InputType.TYPE_CLASS_TEXT
            hint = context.getString(R.string.add_city_hint)
        }

        dialogBuilder.setTitle(getString(R.string.add_city_dialog_title))
            .setView(input)
            .setPositiveButton(R.string.add_city_dialog_positive,
                DialogInterface.OnClickListener { _, _ -> listener?.onDialogPositiveClick(input.text.toString())})

            .setNegativeButton(R.string.add_city_dialog_negative,
                DialogInterface.OnClickListener { _, _ -> listener?.onDialogNegativeClick()})

        return dialogBuilder.create()
    }
}